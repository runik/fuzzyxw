#!/usr/bin/python3.5

import socket, sys, argparse
from time import sleep


parser = argparse.ArgumentParser(description='Simple network fuzzer')

parser.add_argument('host', metavar='RHOST', action="store",
                    help='Remote address for connection')

parser.add_argument('port', metavar='RPORT', action="store", type=int,
                    help='Remote port for connection')


parser.add_argument('-a', '--absolute', dest='absolute', metavar='B', action="store", type=int, default=0,
                    help='Set absolute buffer size')

parser.add_argument('-s', '--step', dest='step', metavar='S', action="store", type=int, default=1024,
                    help='Byte increase of S per round')

parser.add_argument('-r', '--round', dest='rounds', metavar='R', action="store", type=int, default=1024,
                    help='Max number of fuzzing rounds')

parser.add_argument('-b', '--bytes', dest='append', action="store_true", default=False,
                    help='Append bytes from stdin')


args = parser.parse_args()
address = args.host
port = args.port
append = b''

if args.append == True and args.absolute > 0:
    append = bytes(sys.stdin.buffer.read())


def round_fuzz(address, port, step, rounds):
    fuzz = b''
    i = 0
    for i in range(1, rounds):
        fuzz += b'a' * step
        s = socket.socket()
        s.connect((address, port))
        s.recv(2048)
        s.send(fuzz)
        s.recv(2048)
        s.close()
        print( "round {}:\t{} bytes complete".format(i, len(fuzz)) )
    
def absolute_fuzz(address, port, size, append):
    fuzz = b'a' * size
    fuzz += append
    print("Single round of {} bytes".format(len(fuzz)))
    s = socket.socket()
    s.connect((address, port))
    s.recv(2048)
    s.send(fuzz)
    s.close()
    
try:
    if args.absolute == 0:
        round_fuzz(address, port, args.step, args.rounds)
    else:
        absolute_fuzz(address, port, args.absolute, append)
except Exception as e: 
    print("Caught Exception is {} ".format(e))
